# HomeStars

##  Backend Installation
1. Start up your terminal (or Command Prompt on Windows OS).
2. Ensure that you've `ruby 2.6.6` installed on your PC.
3. Clone the repository `https://gitlab.com/stakeholder/homestars.git` and navigate to the root folder.
4. Run the following commands
   ```
   cd homestars_backend
   Run `bundle install`
   Run `bundle exec figaro install`  "this creates a config/application.yml file"
   set the following env variables (db_username, db_password, database, db_host) in the application.yml file
   Run `rails db:setup`
   Run `rails db:seed` to seed channels
   Run `rails server` to start the server

   ```

### API Resource Endpoint


| EndPoint                           | Functionality                                      |
| ---------------------------------- | -------------------------------------------------- |
| **POST** `/register`               | Registers a new account                            |
| **POST** `/login`                  | Logs user in and creates an auth_token for account |
| **GET**  `/channels`               | Returns all open channels                          |
| **GET**  `/channels/:id/join`      | Join a channel                                     |
| **POST**  `/channels/:id/messages` | Post a message to a channel                        |


1. To create a new account
    
    Input:
    ```cmd
    curl -i -X POST -H "Content-Type: application/json" -d '{"firstname":"micheal", "lastname":"power", "email":"noreply@andela.com", "password":"hello", "password_confirmation":"hello"}' http://localhost:3000/register
    ```
    Output:
    ```cmd
    {
       "message": "Account was successfully registered"
    }
    ```


2. To login:
    
    Input:
    ```cmd
    curl -i -X POST -H "Content-Type: application/json" -d '{"email": "user@gmail.com", "password":"pwsd"}' http://localhost:3000/login
    ```
    Output:
    ```
    {
      "auth_token": "eyJhbGciOiJIUzI1NiIsImV4cCI6MTQ5MzE5NDU3MCwiaWF0IjoxNDkzMTkwOTcwfQ.eyJpZCI6MX0.WNgqUeHjLxkzZtVOvuJmrG_OZts_iav5Q2Ogxz5VZR0"
    }
    ```


3. Get all open channels:
    
    Input:
    ```
    curl -i -X GET -H '{"Authorization: eyJhbGciOiJIUzI1NiIsImlhdCI6MTQ5MzExMDQzMCwiZXhwIjoxNDkzMTE0MDMwfQ.eyJ1c2VyX2lkIjozfQ.NvL9u4eAO4iKB8pT501mk-BXx4Kq3p9gcTU83s23Nwo", "Content-Type: application/json"}'  http://localhost:3000/channels
    ```
    Output:
    ```
    {
      "message": "Channels were fetched successfully",
      "status": "success",
      "channels": [
        {
            "id": 21,
            "name": "Channel 0",
            "description": "This is the description for Channel 0",
            "channel_type": "open",
            "created_at": "2021-10-09T15:32:34.256Z",
            "updated_at": "2021-10-09T15:32:34.256Z"
        },
        {
            "id": 22,
            "name": "Channel 1",
            "description": "This is the description for Channel 1",
            "channel_type": "open",
            "created_at": "2021-10-09T15:32:34.261Z",
            "updated_at": "2021-10-09T15:32:34.261Z"
        }
      ]
    }
    ```

4. Join a channel:
    
    Input:
    ```cmd
    curl -i -X GET -H '{"Authorization: eyJhbGciOiJIUzI1NiIsImlhdCI6MTQ5MzExMDQzMCwiZXhwIjoxNDkzMTE0MDMwfQ.eyJ1c2VyX2lkIjozfQ.NvL9u4eAO4iKB8pT501mk-BXx4Kq3p9gcTU83s23Nwo", "Content-Type: application/json"}' http://localhost:3000/channels/3/join
    ```
    Output:
    ```
    {
      "message": "You are now a member of Closed Channel 14"
    }
    ```

5. Persist message in a channel:
    
    Input:
    ```cmd
    curl -i -X POST -H '{"Authorization: eyJhbGciOiJIUzI1NiIsImlhdCI6MTQ5MzExMDQzMCwiZXhwIjoxNDkzMTE0MDMwfQ.eyJ1c2VyX2lkIjozfQ.NvL9u4eAO4iKB8pT501mk-BXx4Kq3p9gcTU83s23Nwo", "Content-Type: application/json"}' -d '{"message_body": "Hello world" }'  http://localhost:3000/channels/3/messages
    ```
    Output:
    ```
    {
      "message": "Message posted successfully",
      "body": {
          "id": 1,
          "body": "Hello world",
          "created_at": "2021-10-09T15:37:20.919Z",
          "updated_at": "2021-10-09T15:37:20.919Z"
      }
    }
    ```



## Few words on which features were selected and why?

Back-End Features
1. As a consumer of the API, I can persist messages in specific channels I join.
2. As a consumer of the API, I can see the list of all the available channels.

The reason why I selected these backend features was because they had an equivalent frontend feature which can consume the endpoints.


## Possible next steps if you had more time. What assumptions did you make, if any?
The next steps will be to implement the frontend features and write more tests in order to increase test coverage.