Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html


  post "/login", to: "authentication#authenticate"
  post "/register", to: "authentication#register"
  get "/channels", to: "channels#index"
  get "channels/:id/join", to: "channels#join"
  post "channels/:id/messages", to: "channels#persist_message"
end
