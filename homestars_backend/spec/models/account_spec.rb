require "rails_helper"

RSpec.describe Account, type: :model do

  describe 'validations' do

    it { should validate_presence_of(:email) }
    it { should validate_presence_of(:password) }
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
    it { should have_secure_password }
  end

  describe 'associations' do
    it { should have_many(:account_channels) }
    it { should have_many(:channels) }
    it { should have_many(:messages) }
  end
end