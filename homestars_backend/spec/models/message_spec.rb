require "rails_helper"

RSpec.describe Message, type: :model do

  describe "validations" do
    it { should validate_presence_of(:account_channel) }
  end

  describe "associations" do
    it { should belong_to(:account_channel) }
  end
end