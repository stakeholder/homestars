require "rails_helper"

RSpec.describe Channel, type: :model do

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:channel_type) }
    it { should define_enum_for(:channel_type) }
  end

  describe 'associations' do
    it { should have_many(:account_channels) }
    it { should have_many(:accounts) }
    it { should have_many(:messages) }
  end

end