require 'rails_helper'

RSpec.describe "AuthenticationRequest", :type => :request do

  describe "#register" do
    let(:headers) { { "ACCEPT" => "application/json", "CONTENT_TYPE" => "application/json"} }
    let(:valid_params) { { email: "test@gmail.com", password: "password", password_confirmation: "password", firstname: "Mark", lastname: "Stevens" } }

    context "when the request is valid" do
      before { post "/register", params: valid_params.to_json, headers: headers }
      it "is successful" do
        expect(response).to have_http_status(:created)
      end

      it "should create a new account" do
        new_account = Account.find_by_email(valid_params[:email])
        expect(new_account).to be_truthy
        expect(new_account.first_name).to eql(valid_params[:firstname])
      end
    end

  end 
  
end