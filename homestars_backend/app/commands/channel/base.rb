class Channel::Base

  def channel_exist?(channel_id)
    Channel.find_by_id(channel_id)
  end

  def member_of_channel?(account, channel_id)
    AccountChannel.where(account_id: account.id, channel_id: channel_id).first
  end


end