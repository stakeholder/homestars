class Channel::PersistMessage < Channel::Base
  prepend SimpleCommand

  attr_reader :account, :channel_id, :message_body

  def initialize(account, channel_id, message_body)
    @account = account
    @channel_id = channel_id
    @message_body = message_body
  end

  def call
    errors.add(:message, channel_member_error) unless member_of_channel?(account, channel_id)

    errors.add(:message, channel_exist_error) unless channel_exist?(channel_id)

    return unless errors.empty?

    persist_message
  end

  private

  def account_channel
    AccountChannel.where(account_id: account.id, channel_id: channel_id).first()
  end


  def persist_message
    Message.create(body: message_body, account_channel_id: account_channel.id)
  end


  def channel_exist_error
    "The channel you are trying to post to is not available."
  end

  def channel_member_error
    "You are not a member of the channel you are trying to post to."
  end
end