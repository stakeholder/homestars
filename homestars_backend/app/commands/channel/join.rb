class Channel::Join < Channel::Base
  prepend SimpleCommand

  attr_reader :account, :channel_id

  def initialize(account, channel_id)
    @account = account
    @channel_id = channel_id
  end

  def call
    errors.add(:message, channel_member_error) if member_of_channel?(account, channel_id)

    errors.add(:message, channel_exist_error) unless channel_exist?(channel_id)
    
    return unless errors.empty?

    join_channel
  end

  private

  def join_channel
    AccountChannel.create(account: account, channel_id: channel_id)
  end

  def channel_exist_error
    "The channel you are trying to join is not available"
  end

  def channel_member_error
    "Already a member of the channel"
  end

end