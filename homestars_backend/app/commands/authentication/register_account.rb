class Authentication::RegisterAccount
  prepend SimpleCommand

  def initialize(registration_params)
    @email = registration_params[:email]
    @firstname = registration_params[:firstname]
    @password = registration_params[:password]
    @password_confirmation = registration_params[:password_confirmation]
    @lastname = registration_params[:lastname]
  end

  def call
    create_acccount if !email_exists? && valid_password?
  end

  private

  attr_reader :email, :password, :password_confirmation, :firstname, :lastname

  def create_acccount
    account = Account.create(
      email: email, 
      password: password, 
      password_confirmation: password_confirmation,
      first_name: firstname,
      last_name: lastname
    )
  end

  def valid_password?
    valid = password == password_confirmation
    unless valid
      errors.add(:password, "Password mismatch")
      return valid
    end
    valid
  end

  def email_exists?
    if Account.find_by_email(email)
      errors.add(:email, "Email has been taken")
      return true
    end
  end
end