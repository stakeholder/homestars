class ChannelSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :channel_type, :created_at, :updated_at
end