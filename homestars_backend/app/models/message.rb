class Message < ApplicationRecord
  belongs_to :account_channel

  validates_presence_of :account_channel
end