class Channel < ApplicationRecord
  enum channel_type: { open: 0, closed: 1 }

  has_many :account_channels
  has_many :accounts, through: :account_channels
  has_many :messages, through: :account_channels

  validates_presence_of :name, :channel_type
  validates_uniqueness_of :name, case_sensitive: false
end