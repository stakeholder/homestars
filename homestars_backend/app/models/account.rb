class Account < ApplicationRecord
  has_secure_password

  has_many :account_channels
  has_many :channels, through: :account_channels
  has_many :messages, through: :account_channels

  validates_presence_of :email, :password, :password_confirmation, :first_name, :last_name
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create
  validates_uniqueness_of :email, case_sensitive: false

end
