class ChannelsController < ApplicationController

  def index
    serializable_resource = ActiveModelSerializers::SerializableResource.new collection
    render json: {message: "Channels were fetched successfully", status: "success", channels: serializable_resource.as_json }, status: :ok
  end

  def join
    command = Channel::Join.call(current_account, join_params[:id])

    if command.success?
      channel = command.result.channel
      render json: { message: "You are now a member of #{channel.name}"}
    else
      render json: { error: command.errors }, status: :bad_request
    end
  end

  def persist_message
    command = Channel::PersistMessage.call(current_account, persist_message_params[:id], persist_message_params[:message_body])

    if command.success?
      serializable_resource = ActiveModelSerializers::SerializableResource.new(command.result, serializer: MessageSerializer)
      render json: { message: "Message posted successfully", body: serializable_resource.as_json }
    else
      render json: { error: command.errors }, status: :bad_request
    end

  end


  private

  def collection
    Channel.where(channel_type: :open)
  end

  def join_params
    params.permit(:id)
  end

  def persist_message_params
    params.permit(%w[id message_body])
  end
end