class AuthenticationController < ApplicationController
  skip_before_action :authenticate_request, only: [:authenticate, :register]


  def register
    command = Authentication::RegisterAccount.call(registration_params)

    if command.success?
      render json: { message: "Account was successfully registered"}, status: :created
    else
      render json: { error: command.errors }, status: :bad_request
    end
  end


  def authenticate
    command = Authentication::AuthenticateAccount.call(account_params[:email], account_params[:password])

    if command.success?
      render json: { auth_token: command.result }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end


  private

  def account_params
    params.permit(:email, :password)
  end

  def registration_params
    params.permit(:email, :password, :password_confirmation, :firstname, :lastname)
  end
end