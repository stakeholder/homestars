class CreateMessages < ActiveRecord::Migration[6.1]
  def change
    create_table :messages do |t|
      t.text :body
      t.references :account_channel, foreign_key: true, null: false

      t.timestamps
    end
  end
end
