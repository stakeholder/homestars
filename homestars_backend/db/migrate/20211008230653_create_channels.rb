class CreateChannels < ActiveRecord::Migration[6.1]
  def change
    create_table :channels do |t|
      t.string :name, null: false, unique: true
      t.text :description, null: false, default: ''
      t.integer :channel_type, null: false, default: 0
      t.references :account, null: true, foreign_key: true

      t.timestamps
    end
  end
end
