class CreateAccountChannels < ActiveRecord::Migration[6.1]
  def change
    create_table :account_channels do |t|
      t.references :account, foreign_key: true, null: false, index: true
      t.references :channel, foreign_key: true, null: false, index: true

      t.timestamps
    end
  end
end
