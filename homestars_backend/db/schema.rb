# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_10_09_130209) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_channels", force: :cascade do |t|
    t.bigint "account_id", null: false
    t.bigint "channel_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_account_channels_on_account_id"
    t.index ["channel_id"], name: "index_account_channels_on_channel_id"
  end

  create_table "accounts", force: :cascade do |t|
    t.string "email", null: false
    t.string "password_digest", null: false
    t.string "first_name", null: false
    t.string "last_name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "channels", force: :cascade do |t|
    t.string "name", null: false
    t.text "description", default: "", null: false
    t.integer "channel_type", default: 0, null: false
    t.bigint "account_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_id"], name: "index_channels_on_account_id"
  end

  create_table "messages", force: :cascade do |t|
    t.text "body"
    t.bigint "account_channel_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["account_channel_id"], name: "index_messages_on_account_channel_id"
  end

  add_foreign_key "account_channels", "accounts"
  add_foreign_key "account_channels", "channels"
  add_foreign_key "channels", "accounts"
  add_foreign_key "messages", "account_channels"
end
